#include <hpcdefs.hpp>
#include <image.hpp>



void convert_rgb_to_grayscale_optimized(const uint8_t *CSE6230_RESTRICT rgb_image, uint8_t *CSE6230_RESTRICT grayscale_image, size_t width, size_t height) {

               const __m128i rconst = _mm_set1_epi16(54);
                 const __m128i gconst = _mm_set1_epi16(183);
                const __m128i bconst = _mm_set1_epi16(19);
		size_t size = (width*height);
		size_t ptr=0;
		__m128i rgb1 = _mm_setzero_si128();
		__m128i rgb2 = _mm_setzero_si128();
		__m128i rgb3 = _mm_setzero_si128();
		__m128i rgb4 = _mm_setzero_si128();

		//__m128i gray1, gray2, gray3, gray4;		
		while(ptr <= (size-32))
		{
		/*__m128i ssse3_red_indeces_0 = _mm_set_epi8(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 15, 12, 9, 6, 3, 0);
		__m128i ssse3_red_indeces_1 = _mm_set_epi8(-1, -1, -1, -1, -1, 14, 11, 8, 5, 2, -1, -1, -1, -1, -1, -1);
		__m128i ssse3_red_indeces_2 = _mm_set_epi8(13, 10, 7, 4, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1);
		__m128i ssse3_green_indeces_0 = _mm_set_epi8(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 13, 10, 7, 4, 1);
		__m128i ssse3_green_indeces_1 = _mm_set_epi8(1, -1, -1, -1, -1, 15, 12, 9, 6, 3, 0, -1, -1, -1, -1, -1);
		__m128i ssse3_green_indeces_2 = _mm_set_epi8(14, 11, 8, 5, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1);
		__m128i ssse3_blue_indeces_0 = _mm_set_epi8(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 14, 11, 8, 5, 2);
		__m128i ssse3_blue_indeces_1 = _mm_set_epi8(-1, -1, -1, -1, -1, -1, 13, 10, 7, 4, 1, -1, -1, -1, -1, -1);
		__m128i ssse3_blue_indeces_2 = _mm_set_epi8(15, 12, 9, 6, 3, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1);
		const __m128i chunk0 = _mm_loadu_si128((const __m128i*)(rgb_image + 3*ptr));
		const __m128i chunk1 = _mm_loadu_si128((const __m128i*)(rgb_image + 3*ptr + 16 ));
		const __m128i chunk2 = _mm_loadu_si128((const __m128i*)(rgb_image + 3*ptr + 32));
//		rgb_image += 48;
		const __m128i red = _mm_or_si128(_mm_or_si128(_mm_shuffle_epi8(chunk0, ssse3_red_indeces_0),_mm_shuffle_epi8(chunk1, ssse3_red_indeces_1)),_mm_shuffle_epi8(chunk2, ssse3_red_indeces_2));
		const __m128i green = _mm_or_si128(_mm_or_si128(_mm_shuffle_epi8(chunk0, ssse3_green_indeces_0),_mm_shuffle_epi8(chunk1, ssse3_green_indeces_1)), _mm_shuffle_epi8(chunk2, ssse3_green_indeces_2));
		const __m128i blue = _mm_or_si128(_mm_or_si128(_mm_shuffle_epi8(chunk0, ssse3_blue_indeces_0),_mm_shuffle_epi8(chunk1, ssse3_blue_indeces_1)),_mm_shuffle_epi8(chunk2, ssse3_blue_indeces_2));

		 __m128i red1 = _mm_set_epi16(_mm_extract_epi8(red,0),_mm_extract_epi8(red,1),_mm_extract_epi8(red,2),_mm_extract_epi8(red,3),_mm_extract_epi8(red,4),_mm_extract_epi8(red,5),_mm_extract_epi8(red,6),_mm_extract_epi8(red,7));
		 __m128i red2 = _mm_set_epi16(_mm_extract_epi8(red,8),_mm_extract_epi8(red,9),_mm_extract_epi8(red,10),_mm_extract_epi8(red,11),_mm_extract_epi8(red,12),_mm_extract_epi8(red,13),_mm_extract_epi8(red,14),_mm_extract_epi8(red,15));
		 __m128i green1 = _mm_set_epi16(_mm_extract_epi8(green,0),_mm_extract_epi8(green,1),_mm_extract_epi8(green,2),_mm_extract_epi8(green,3),_mm_extract_epi8(green,4),_mm_extract_epi8(green,5),_mm_extract_epi8(green,6),_mm_extract_epi8(green,7));
		 __m128i green2 = _mm_set_epi16(_mm_extract_epi8(green,8),_mm_extract_epi8(green,9),_mm_extract_epi8(green,10),_mm_extract_epi8(green,11),_mm_extract_epi8(green,12),_mm_extract_epi8(green,13),_mm_extract_epi8(green,14),_mm_extract_epi8(green,15));
		 __m128i blue1 =  _mm_set_epi16(_mm_extract_epi8(blue,0),_mm_extract_epi8(blue,1),_mm_extract_epi8(blue,2),_mm_extract_epi8(blue,3),_mm_extract_epi8(blue,4),_mm_extract_epi8(blue,5),_mm_extract_epi8(blue,6),_mm_extract_epi8(blue,7));
		 __m128i blue2 =  _mm_set_epi16(_mm_extract_epi8(blue,8),_mm_extract_epi8(blue,9),_mm_extract_epi8(blue,10),_mm_extract_epi8(blue,11),_mm_extract_epi8(blue,12),_mm_extract_epi8(blue,13),_mm_extract_epi8(blue,14),_mm_extract_epi8(blue,15));
		
		red1=_mm_mullo_epi16(red1,rconst);
		red2=_mm_mullo_epi16(red2,rconst);
		red1=_mm_srli_si128(red1,8);		
		//red2=_mm_srli_si128(red2,8);
		__m128i red11=_mm_or_si128(red1,red2);
		
		_mm_storeu_si128((__m128i*)grayscale_image,red11); 


	ptr+=16;
	}*/




//        for (size_t i = 0; i < height; i++) {
//                for (size_t ptr = 0; ptr < (width*height-32); ptr+=32) {
                	const __m128i base = _mm_loadu_si128((const __m128i*)(rgb_image+(3*(ptr))));
                        const __m128i base2 = _mm_loadu_si128((const __m128i*)(rgb_image+3*(ptr)+16));
                        const __m128i base3 = _mm_loadu_si128((const __m128i*)(rgb_image+3*(ptr)+32));
                        const __m128i base4 = _mm_loadu_si128((const __m128i*)(rgb_image+3*(ptr)+48));
                        const __m128i base5 = _mm_loadu_si128((const __m128i*)(rgb_image+3*(ptr)+64));
                        const __m128i base6 = _mm_loadu_si128((const __m128i*)(rgb_image+3*(ptr)+80));
        
			__m128i lvl11 = _mm_unpacklo_epi8(base, base4);
                        __m128i lvl12 = _mm_unpackhi_epi8(base, base4);
                        __m128i lvl13 = _mm_unpacklo_epi8(base2, base5);
                        __m128i lvl14 = _mm_unpackhi_epi8(base2, base5);
                        __m128i lvl15 = _mm_unpacklo_epi8(base3, base6);
                        __m128i lvl16 = _mm_unpackhi_epi8(base3, base6);

                        __m128i lvl21 = _mm_unpacklo_epi8(lvl11, lvl14);
                        __m128i lvl22 = _mm_unpackhi_epi8(lvl11, lvl14);
                        __m128i lvl23 = _mm_unpacklo_epi8(lvl12, lvl15);
                        __m128i lvl24 = _mm_unpackhi_epi8(lvl12, lvl15);
                        __m128i lvl25 = _mm_unpacklo_epi8(lvl13, lvl16);
                        __m128i lvl26 = _mm_unpackhi_epi8(lvl13, lvl16);

                        __m128i lvl31 = _mm_unpacklo_epi8(lvl21, lvl24);
                        __m128i lvl32 = _mm_unpackhi_epi8(lvl21, lvl24);
                        __m128i lvl33 = _mm_unpacklo_epi8(lvl22, lvl25);
                        __m128i lvl34 = _mm_unpackhi_epi8(lvl22, lvl25);
			__m128i lvl35 = _mm_unpacklo_epi8(lvl23, lvl26);
                        __m128i lvl36 = _mm_unpackhi_epi8(lvl23, lvl26);
		
			__m128i lvl41 = _mm_unpacklo_epi8(lvl31, lvl34);
                        __m128i lvl42 = _mm_unpackhi_epi8(lvl31, lvl34);
                        __m128i lvl43 = _mm_unpacklo_epi8(lvl32, lvl35);
                        __m128i lvl44 = _mm_unpackhi_epi8(lvl32, lvl35);
                        __m128i lvl45 = _mm_unpacklo_epi8(lvl33, lvl36);
                        __m128i lvl46 = _mm_unpackhi_epi8(lvl33, lvl36);

			__m128i lvl51 = _mm_unpacklo_epi8(lvl41, lvl44);
                        __m128i lvl52 = _mm_unpackhi_epi8(lvl41, lvl44);
                        __m128i lvl53 = _mm_unpacklo_epi8(lvl42, lvl45);
                        __m128i lvl54 = _mm_unpackhi_epi8(lvl42, lvl45);
                        __m128i lvl55 = _mm_unpacklo_epi8(lvl43, lvl46);
                        __m128i lvl56 = _mm_unpackhi_epi8(lvl43, lvl46);
			
			
			/*__m128i red1 = _mm_unpacklo_epi8(lvl51,lvl51);
			__m128i red2 = _mm_unpackhi_epi8(lvl51,lvl51);
			__m128i red3 = _mm_unpacklo_epi8(lvl52,lvl52);
			__m128i red4 = _mm_unpackhi_epi8(lvl52,lvl52);
			__m128i green1 = _mm_unpacklo_epi8(lvl53,lvl53);
			 __m128i green2 = _mm_unpackhi_epi8(lvl53,lvl53);
			 __m128i green3 = _mm_unpacklo_epi8(lvl54,lvl54);
			 __m128i green4 = _mm_unpackhi_epi8(lvl54,lvl54);
			 __m128i blue1 = _mm_unpacklo_epi8(lvl55,lvl55);
			 __m128i blue2 = _mm_unpackhi_epi8(lvl55,lvl55);
			 __m128i blue3 = _mm_unpacklo_epi8(lvl56,lvl56);
			 __m128i blue4 = _mm_unpacklo_epi8(lvl56,lvl56);
			red1 = _mm_srli_si128(red1,8);
			red2 = _mm_srli_si128(red2,8);
			red3 = _mm_srli_si128(red3,8);
			red4 = _mm_srli_si128(red4,8);
			green1 = _mm_srli_si128(green1,8);
			green2 = _mm_srli_si128(green2,8);
			 green3 = _mm_srli_si128(green3,8);
			 green4 = _mm_srli_si128(green4,8);
			blue1 = _mm_srli_si128(blue1,8);
			blue2 = _mm_srli_si128(blue2,8);
			blue3 = _mm_srli_si128(blue3,8);
			blue4 = _mm_srli_si128(blue4,8);*/

			__m128i red1 = _mm_set_epi16(_mm_extract_epi8(lvl51,7),_mm_extract_epi8(lvl51,6),_mm_extract_epi8(lvl51,5),_mm_extract_epi8(lvl51,4),_mm_extract_epi8(lvl51,3),_mm_extract_epi8(lvl51,2),_mm_extract_epi8(lvl51,1),_mm_extract_epi8(lvl51,0));
			__m128i red2 = _mm_set_epi16(_mm_extract_epi8(lvl51,15),_mm_extract_epi8(lvl51,14),_mm_extract_epi8(lvl51,13),_mm_extract_epi8(lvl51,12),_mm_extract_epi8(lvl51,11),_mm_extract_epi8(lvl51,10),_mm_extract_epi8(lvl51,9),_mm_extract_epi8(lvl51,8));
			__m128i red3 = _mm_set_epi16(_mm_extract_epi8(lvl52,7),_mm_extract_epi8(lvl52,6),_mm_extract_epi8(lvl52,5),_mm_extract_epi8(lvl52,4),_mm_extract_epi8(lvl52,3),_mm_extract_epi8(lvl52,2),_mm_extract_epi8(lvl52,1),_mm_extract_epi8(lvl52,0));
                      __m128i red4 = _mm_set_epi16(_mm_extract_epi8(lvl52,15),_mm_extract_epi8(lvl52,14),_mm_extract_epi8(lvl52,13),_mm_extract_epi8(lvl52,12),_mm_extract_epi8(lvl52,11),_mm_extract_epi8(lvl52,10),_mm_extract_epi8(lvl52,9),_mm_extract_epi8(lvl52,8));
			
                       __m128i green1 = _mm_set_epi16(_mm_extract_epi8(lvl53,7),_mm_extract_epi8(lvl53,6),_mm_extract_epi8(lvl53,5),_mm_extract_epi8(lvl53,4),_mm_extract_epi8(lvl53,3),_mm_extract_epi8(lvl53,2),_mm_extract_epi8(lvl53,1),_mm_extract_epi8(lvl53,0));
                        __m128i green2 = _mm_set_epi16(_mm_extract_epi8(lvl53,15),_mm_extract_epi8(lvl53,14),_mm_extract_epi8(lvl53,13),_mm_extract_epi8(lvl53,12),_mm_extract_epi8(lvl53,11),_mm_extract_epi8(lvl53,10),_mm_extract_epi8(lvl53,9),_mm_extract_epi8(lvl53,8));
                        __m128i green3 = _mm_set_epi16(_mm_extract_epi8(lvl54,7),_mm_extract_epi8(lvl54,6),_mm_extract_epi8(lvl54,5),_mm_extract_epi8(lvl54,4),_mm_extract_epi8(lvl54,3),_mm_extract_epi8(lvl54,2),_mm_extract_epi8(lvl54,1),_mm_extract_epi8(lvl54,0));
                      __m128i green4 = _mm_set_epi16(_mm_extract_epi8(lvl54,15),_mm_extract_epi8(lvl54,14),_mm_extract_epi8(lvl54,13),_mm_extract_epi8(lvl54,12),_mm_extract_epi8(lvl54,11),_mm_extract_epi8(lvl54,10),_mm_extract_epi8(lvl54,9),_mm_extract_epi8(lvl54,8));

			 __m128i blue1 = _mm_set_epi16(_mm_extract_epi8(lvl55,7),_mm_extract_epi8(lvl55,6),_mm_extract_epi8(lvl55,5),_mm_extract_epi8(lvl55,4),_mm_extract_epi8(lvl55,3),_mm_extract_epi8(lvl55,2),_mm_extract_epi8(lvl55,1),_mm_extract_epi8(lvl55,0));
                        __m128i blue2 = _mm_set_epi16(_mm_extract_epi8(lvl55,15),_mm_extract_epi8(lvl55,14),_mm_extract_epi8(lvl55,13),_mm_extract_epi8(lvl55,12),_mm_extract_epi8(lvl55,11),_mm_extract_epi8(lvl55,10),_mm_extract_epi8(lvl55,9),_mm_extract_epi8(lvl55,8));
                        __m128i blue3 = _mm_set_epi16(_mm_extract_epi8(lvl56,7),_mm_extract_epi8(lvl56,6),_mm_extract_epi8(lvl56,5),_mm_extract_epi8(lvl56,4),_mm_extract_epi8(lvl56,3),_mm_extract_epi8(lvl56,2),_mm_extract_epi8(lvl56,1),_mm_extract_epi8(lvl56,0));
                      __m128i blue4 = _mm_set_epi16(_mm_extract_epi8(lvl56,15),_mm_extract_epi8(lvl56,14),_mm_extract_epi8(lvl56,13),_mm_extract_epi8(lvl56,12),_mm_extract_epi8(lvl56,11),_mm_extract_epi8(lvl56,10),_mm_extract_epi8(lvl56,9),_mm_extract_epi8(lvl56,8));


                      	/*red1 = _mm_srli_epi16(red1,8);
			red2 = _mm_srli_epi16(red2,8);
			red3 = _mm_srli_epi16(red3,8);
			red4 = _mm_srli_epi16(red4,8);
			green1 = _mm_srli_epi16(green1,8);
			green2 = _mm_srli_epi16(green2,8);
			green3 = _mm_srli_epi16(green3,8);
			green4 = _mm_srli_epi16(green4,8);
			blue1 = _mm_srli_epi16(blue1,8);
			blue2 = _mm_srli_epi16(blue2,8);
			blue3 = _mm_srli_epi16(blue3,8);
			blue4 = _mm_srli_epi16(blue4,8);
			*/
			rgb1 = _mm_mullo_epi16(red1 , rconst);
                        rgb1 = _mm_add_epi16(rgb1,_mm_mullo_epi16(green1, gconst));
                        rgb1 = _mm_add_epi16(rgb1,_mm_mullo_epi16(blue1, bconst));
                        rgb2 = _mm_mullo_epi16(red2, rconst);
                        rgb2 = _mm_add_epi16(rgb2,_mm_mullo_epi16(green2, gconst));
                        rgb2 = _mm_add_epi16(rgb2,_mm_mullo_epi16(blue2, bconst));
			rgb3 = _mm_mullo_epi16(red3, rconst);
                        rgb3 = _mm_add_epi16(rgb3,_mm_mullo_epi16(green3, gconst));
                        rgb3 = _mm_add_epi16(rgb3,_mm_mullo_epi16(blue3, bconst));
			rgb4 = _mm_mullo_epi16(red4, rconst);
                        rgb4 = _mm_add_epi16(rgb4,_mm_mullo_epi16(green4, gconst));
                        rgb4 = _mm_add_epi16(rgb4,_mm_mullo_epi16(blue4, bconst));
			
			
			__m128i rgb11 = _mm_srli_epi16(rgb1,8);
                        __m128i rgb12 = _mm_srli_epi16(rgb2,8);
                        __m128i rgb13 = _mm_srli_epi16(rgb3,8);
                        __m128i rgb14 = _mm_srli_epi16(rgb4,8);

			/*shuffle instead of unpack*/
			__m128i gray_mask1 = _mm_set_epi8(-1,-1,-1,-1,-1,-1,-1,-1,14,12,10,8,6,4,2,0);
			__m128i gray_mask2 = _mm_set_epi8(14,12,10,8,6,4,2,0,-1,-1,-1,-1,-1,-1,-1,-1);
			__m128i gray1 = _mm_or_si128(_mm_shuffle_epi8(rgb11,gray_mask1),_mm_shuffle_epi8(rgb12,gray_mask2));
			__m128i gray2 = _mm_or_si128(_mm_shuffle_epi8(rgb13,gray_mask1),_mm_shuffle_epi8(rgb14,gray_mask2));
			
			/*__m128i gray1 = _mm_unpacklo_epi8(rgb11,rgb12);
			__m128i gray2 = _mm_unpackhi_epi8(rgb11,rgb12);
			__m128i gray3 = _mm_unpacklo_epi8(rgb13,rgb14);
			__m128i gray4 = _mm_unpackhi_epi8(rgb13,rgb14);
			__m128i gray21 = _mm_unpacklo_epi8(gray1,gray2);
			__m128i gray22 = _mm_unpackhi_epi8(gray1, gray2);
			__m128i gray23 = _mm_unpacklo_epi8(gray3,gray4);
			__m128i gray24 = _mm_unpackhi_epi8(gray3,gray4);
			__m128i gray31 = _mm_unpacklo_epi8(gray21,gray22);
			__m128i gray32 = _mm_unpackhi_epi8(gray21,gray22);
			__m128i gray33 = _mm_unpacklo_epi8(gray23,gray24);
			__m128i gray34 = _mm_unpackhi_epi8(gray23,gray24);
			__m128i gray41 = _mm_unpacklo_epi8(gray31, gray32);
			__m128i gray42 = _mm_unpackhi_epi8(gray31, gray32);
			__m128i gray43 = _mm_unpacklo_epi8(gray33, gray34);
			__m128i gray44 = _mm_unpackhi_epi8(gray33, gray34);*/



                        //green1 = _mm_srli_epi16(green1,8);
                        //green2 = _mm_srli_epi16(green2,8);
                        //green3 = _mm_srli_epi16(green3,8);
                        //green4 = _mm_srli_epi16(green4,8);
                        //blue1 = _mm_srli_epi16(blue1,8);
                        //blue2 = _mm_srli_epi16(blue2,8);
                        //blue3 = _mm_srli_epi16(blue3,8);
                        //blue4 = _mm_srli_epi16(blue4,8);
                        
		//	const __m128i rgb21 = _mm_or_si128(rgb11,rgb2);
		//	const __m128i rgb22 = _mm_or_si128(rgb12,rgb4);
			 /*const __m128i green11 = _mm_or_si128(green1,green2);
			 const __m128i green12 = _mm_or_si128(green3,green4);
			 const __m128i blue11 = _mm_or_si128(blue1,blue2);
			 const __m128i blue12 = _mm_or_si128(blue3,blue4);
			*/
			

			/*__m128i gray1 = _mm_add_epi8(red11,green11);
			gray1 = _mm_add_epi8(gray1,blue11);
			__m128i gray2 = _mm_add_epi8(red12,green12);
			gray2 = _mm_add_epi8(gray2,blue12);*/
			/*gray3 = _mm_add_epi16(red3, green3);
			gray3 = _mm_add_epi16(gray3, blue3);
			gray4 = _mm_add_epi16(red4,green4);
			gray4 = _mm_add_epi16(gray4, blue4);
			*/	
/*	//			gray1 = _mm_slli_epi16(gray1,8);

	//		gray3 = _mm_slli_epi16(gray3,8);
			//__m128i gray = _mm_srli_epi16(gray11,8);
			//__m128i gray2 = _mm_srli_epi16(gray21,0);
			//__m128i gray3 = _mm_srli_epi16(gray31,8);
			//__m128i gray4 = _mm_srli_epi16(gray41,0);
			gray1 = _mm_or_si128(gray1,gray2);
			gray3 = _mm_or_si128(gray3,gray4);
			

		//	__m128i gray81 = _mm_unpacklo_epi16
	//		__m128i gray81=_mm_set_epi8(_mm_extract_epi16(gray,0),_mm_extract_epi16(gray,1),_mm_extract_epi16(gray,2),_mm_extract_epi16(gray,3),_mm_extract_epi16(gray,4),_mm_extract_epi16(gray,5),_mm_extract_epi16(gray,6),_mm_extract_epi16(gray,7),_mm_extract_epi16(gray2,0),_mm_extract_epi16(gray2,1),_mm_extract_epi16(gray2,2),_mm_extract_epi16(gray2,3),_mm_extract_epi16(gray2,4),_mm_extract_epi16(gray2,5),_mm_extract_epi16(gray2,6),_mm_extract_epi16(gray2,7));
	//		__m128i gray82=_mm_set_epi8(_mm_extract_epi16(gray3,0),_mm_extract_epi16(gray3,1),_mm_extract_epi16(gray3,2),_mm_extract_epi16(gray3,3),_mm_extract_epi16(gray3,4),_mm_extract_epi16(gray3,5),_mm_extract_epi16(gray3,6),_mm_extract_epi16(gray3,7),_mm_extract_epi16(gray4,0),_mm_extract_epi16(gray4,1),_mm_extract_epi16(gray4,2),_mm_extract_epi16(gray4,3),_mm_extract_epi16(gray4,4),_mm_extract_epi16(gray4,5),_mm_extract_epi16(gray4,6),_mm_extract_epi16(gray4,7));

			//gray81=_mm_srli_si128(gray81,8);
			//gray82=_mm_srli_si128(gray82,8);

			//for( k=0, p=0;k<48,p<16;k+=3,++p)
			//{
			//grayscale_image[i*width+j+k]=__mm_extract_epi8(gray81,p);
			//grayscale_image[i*width+j+k+16]=__mm_extract_epi8(gray82,p);
			//grayscale_image[i*width+j+16]=gray82;
				//grayscale_image[i*width+j+k+16]=_mm_extract_epi8(lvl54,k);

		//	}*/
			_mm_storeu_si128((__m128i*)grayscale_image,gray1);
                      _mm_storeu_si128((__m128i*)(grayscale_image+16),gray2);

			ptr +=32;
			grayscale_image += 32;
                }
       // }
}


void integrate_image_optimized(const uint8_t *CSE6230_RESTRICT source_image, uint32_t *CSE6230_RESTRICT integral_image, size_t width, size_t height) {
	for (size_t i = 0; i < height; i++) {
		uint32_t integral = 0;
		for (size_t j = 0; j < width; j++) {
			integral += source_image[i * width + j];
			integral_image[i * width + j] = integral;
		}
	}
	for (size_t j = 0; j < width; j++) {
		uint32_t integral = 0;
		for (size_t i = 0; i < height; i++) {
			integral += integral_image[i * width + j];
			integral_image[i * width + j] = integral;
		}
	}
}

/*
void integrate_image_optimized(const uint8_t *CSE6230_RESTRICT source_image, uint32_t *CSE6230_RESTRICT integral_image, size_t width, size_t height) {

//	uint32_t integral = 0;

        for (size_t i = 0; i < height; i++) {
  		 uint32_t integral = 0;            
            for (size_t j = 0; j < (width); j+=1) {
*/
		/*	integral[0] = (source_image[i*width+j]);
			integral[1] = (source_image[i*width+j+1]);
			integral[2] = (source_image[i*width+j+2]);
			integral[3] = (source_image[i*width+j+3]);
			__m128i base = _mm_loadu_si128((__m128i*)(*(integral)));
			__m128i base2 = _mm_srli_si128(base,4);
			__m128i base3 = _mm_add_epi32(base, base2);
			__m128i base4 = _mm_srli_si128(base3,4);
			__m128i base5 = _mm_add_epi32(base3, base4); 
			 integral += source_image[i * width + j];
//			_mm_storeu_si128((__m128i*)((integral_image+(i*width+j))),base5);
                        integral_image[i * width + j] = integral;
                }
        }
        for (size_t j = 0; j < width; j++) {
                uint32_t integral = 0;
                for (size_t i = 0; i < height; i+=1) {*/
		/*	__m128i base = _mm_loadu_si128((__m128i*)(*(integral_image+(i*width+j))));
                        __m128i base2 = _mm_srli_si128(base,4);
                        base = _mm_add_epi32(base, base2);
                        base2 = _mm_srli_si128(base,4);
                        base = _mm_add_epi32(base, base2);*/
			 //_mm_storeu_si128((__m128i*)((integral_image+(i*width+j))),base);
/*
                      integral += integral_image[i * width + j];
                     integral_image[i * width + j] = integral;
                }
        }
}
  */                      
